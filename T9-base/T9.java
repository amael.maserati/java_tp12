import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.HashMap;
import java.util.Map;

class T9{
    private HashMap<String,Double> dicoFreq;
    private Map<String, String> prefixeMot;
    /**
      * @param mot
      * une chaine de caractère
      * @return une chaine de caractères correspondant à la suite de touches à taper pour obtenir le mot en entrée.
      *  Par exemple pour le mot 'toto', la fonction renvoie '8686'
      */
    public static String toTouches(String mot){
        Map<String, Integer> alphabet = new HashMap<String, Integer>();
        alphabet.put("a", 2);
        alphabet.put("b", 2);
        alphabet.put("c", 2);
        alphabet.put("d", 3);
        alphabet.put("e", 3);
        alphabet.put("f", 3);
        alphabet.put("g", 4);
        alphabet.put("h", 4);
        alphabet.put("i", 4);
        alphabet.put("j", 5);
        alphabet.put("k", 5);
        alphabet.put("l", 5);
        alphabet.put("m", 6);
        alphabet.put("n", 6);
        alphabet.put("o", 6);
        alphabet.put("p", 7);
        alphabet.put("q", 7);
        alphabet.put("r", 7);
        alphabet.put("s", 7);
        alphabet.put("t", 8);
        alphabet.put("u", 8);
        alphabet.put("v", 8);
        alphabet.put("w", 9);
        alphabet.put("x", 9);
        alphabet.put("y", 9);
        alphabet.put("z", 9);
        String touches = "";
        for (int i = 0; i < mot.length(); ++i) {
            touches += alphabet.get(String.valueOf(mot.charAt(i)));
        }
        return touches;
    }
    /**
     * Constructeur
     */
    T9()
    {
        try{
            this.dicoFreq = new HashMap<String,Double>();
            this.prefixeMot = new HashMap<String,String>();
            Scanner in = new Scanner(new FileReader("liste_mots.txt")).useLocale(Locale.US);
            while(in.hasNext()){
                String s=in.next();
                double d=in.nextDouble();
                dicoFreq.put(s,d);
                // System.out.println("le mot " + s + " a la valeur " + d);
                this.prefixeMot.put(toTouches(s), s);
            }
        }
        catch(FileNotFoundException e){
            System.out.println("Pas de fichier");
        }
    }
    /**
      * @param prefixe
      *     une chaine de caractères correspondant à une suite de touches tapées, par exemple "8686"
      * @return le mot le plus probable correspondant à la suite de touches.
      */
    public String getMot(String prefixe){
        if (this.prefixeMot.containsKey(prefixe)) {
            return this.prefixeMot.get(prefixe);
        }
        else {
            return "";
        }
    }
    /**
      * @param numeroTouche
      *      un entier correspondant à la position de la touche sur le clavier (par exemple la touche '2' est à la position 1.)
      * @return le symbole qui doit être affiché sur la touche. Par exemple, sur l'entrée 1, le symbole est '2', sur l'entrée 9, le symbole est '*'
    */
    public String getSymbole(int numeroTouche){
        // TODO Q1.2 et Q1.3
        if (numeroTouche == 9) {
            return "*";
        }
        if (numeroTouche == 10) {
            return "0";
        }
        if (numeroTouche == 11) {
            return "#";
        }
        else {
            return String.valueOf(numeroTouche + 1);
        }
    }

    /**
      * @param numeroTouche
      *      un entier correspondant à la position de la touche sur le clavier (par exemple la touche '2' est à la position 1.)
      * @return les lettres qui correspondent à la touche. Par exemple, sur l'entrée 1, la fonction renvoie "abc" qui correpond au texte de la touche 2.
    */

    public String getTexte(int numeroTouche){
        if (numeroTouche == 0 || numeroTouche == 10) {
            return "";
        }
        if (numeroTouche == 1) {
            return "abc";
        }
        if (numeroTouche == 2) {
            return "def";
        }
        if (numeroTouche == 3) {
            return "ghi";
        }
        if (numeroTouche == 4) {
            return "jkl";
        }
        if (numeroTouche == 5) {
            return "mno";
        }
        if (numeroTouche == 6) {
            return "pqrs";
        }
        if (numeroTouche == 7) {
            return "tuv";
        }
        if (numeroTouche == 8) {
            return "wxyz";
        }
        return "";
    }
}
